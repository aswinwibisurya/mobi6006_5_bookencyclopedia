package com.example.aswin.a5_bookencyclopedia;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class BooksAdapter extends RecyclerView.Adapter<BooksAdapter.ViewHolder> {

    Context mCtx;
    ArrayList<Book> listBooks;

    public BooksAdapter(Context ctx, ArrayList<Book> listBooks) {
        mCtx = ctx;
        this.listBooks = listBooks;
    }

    public void addBook(Book book) {
        listBooks.add(book);

        notifyItemInserted(listBooks.size() - 1);

//        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        Book book = listBooks.get(position);
        if(book.getTitle().startsWith("Java")) {
            return 0;
        } else {
            return 1;
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View v = inflater.inflate(R.layout.item_row_book, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Book book = listBooks.get(position);

        holder.ivThumbnail.setImageResource(book.getThumbnail());
        holder.tvTitle.setText(book.getTitle());
    }

    @Override
    public int getItemCount() {
        return listBooks.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView ivThumbnail;
        TextView tvTitle;
        public ViewHolder(View itemView) {
            super(itemView);
            ivThumbnail = itemView.findViewById(R.id.ivThumbnail);
            tvTitle = itemView.findViewById(R.id.tvTitle);
        }
    }
}
